variable ecr_name {
    default = "pe-app" 
}

variable image_tag_mutability {
    default = "MUTABLE"
}
